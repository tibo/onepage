
# #CITOYEN Engagé
- Acteur et sentinelle locale 
- Ne transmet pas ses droits
- Assume son role citoyen
- Fait des propositions librement 

## CITOYEN Connecté 
- Acteur Temps réél
- Contributeur illimité
- Informé sur tout 
- Voter sur tout 
- Connecté aux acteurs et aux lieux du territoire

## CITOYEN REsponsable
- Ne délègue pas le pouvoir 
- Utilise son pouvoir quotidiennement 
- Ne critique pas mais construit son environnement politique

## Démocratie
- Réélle
- Peut être liquide
- Questionner la représentativité

## Justice
- Adapté
- Pas de double vitesse 
- Justice Agile 
- Pas de préférence hiérarchique

## Ecole de la Citoyenneté
- Se retrouver pour mieux agir 
- Motiver l’autonomie et la résilience 
- Apprendre ses droits et devoir citoyens
- Respect des droits de l’homme et de la nature

## Experimenter
- Questionner 
- Créer des zones d’experimentations ()
- Proposer des alternatives 
- Agilité politique
- Amateurisme politique
- Tirage au sort 
- Débat citoyen

## Outils
- Utiliser les outils de son temps

## Attention
- Technoverdose
- Accaparation / Vigilance constante 
- Rester fidèle à ses valeurs
- N’ayons pas peur du changement

## Proverbe
Sois le changement que tu veux voir dans le monde
MAHATMA GANDHI
