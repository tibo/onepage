# Déchets 
Territoire Zéro Déchet

Intégrer un projet de société locale pour les générations futures 
qui prend en compte l’obsolescence et la finitude des ressources 

## ACtions Personnelles
- Préférer les produits non suremballés (vrac, légumes, marché…)
- Prendre cabat, barquette réutilisable, paniers… à proposer à son commerçant pour remplacer son contenant jetable
- Participer ou organiser un défi famille zéro déchet


## PLATEFORME ZERO WASTE DE QUARTIER 
- Composteur collectif avec maître composteur
- Broyeur de végétaux partagé
- Bibliothèque d’outils - économie de la fonctionnalité 
- Reparali Kafé régulier pour réparer soit même et éviter de jeter 
- Epicerie coopérative / collaborative vrac et local
- Espace de don d’objets
- Adapté en fonction des besoins citoyens
- Fablab  - Fabrication d’objets (meubles, électroménager) en open hardware


## Générateur d’emplois
- Sas de décheterie / Recyclerie : avant de jeter essayer de réparer / réutiliser
- Ressourcerie : créer une bibliothèque de pièces détachées
- Concours d’éco-conception d’objets produit localement sans déchet
- Filières de recyclage local (avec coopération région OI)
- Citoyen Récoltant 
- Valoriser la production locale ( centre de transformation partagé )

## Alimentation
- Production ultra locale interconnectée / jardin partagé urbain
- Légumes frais et localement produit (pas de suremballage)
- Créer le besoin d’un compost

## Education 
- Mise en place de cours de gestion des déchets dès la petite enfance
- Ludification du tri

## VALORISATION DES DECHETS 
- Consigne bouteille
- Donner une valeur aux objets qui peuvent être recyclés (métaux, cartons, plastiques, batteries…)
- Réseau de poubelle à tri, en sortie de supermarché et point de récolte en commune

## EVITER LE GASPILLAGE
- Cours de cuisine des restes + manuel de l’antigaspi
- Compost / Déshydrateur dans les restaurants / cantines

## ENCOURAGEMENTS
- Tarification incitative : moins je produis de déchets, moins je paie
- Faire la promotion des couches lavables (50% de moins sur la TOM pour les familles utilisant les couches lavables)
- Monnaie locale éco-citoyenne : un éco-geste = un point éco-citoyen échangeable
- Police de l’environnement régionale pour synchroniser les actions des polices départementales et municipales

## Proverbe
Je ne veux pas protéger l’environnement,
Je veux créer un monde dans lequel
L’environnement n’a pas besoin d’etre protégé
GREENPEACE
